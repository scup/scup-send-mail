const dependencies = {
  AWS: require('aws-sdk')
}

const SESClient = {
  sendEmail ({
    from,
    destinations,
    subject,
    content,
    awsRegion
  }, injection) {
    const { AWS } = Object.assign({}, dependencies, injection)

    const SES = new AWS.SES({ region: awsRegion })

    const params = {
      Destination: {
        ToAddresses: destinations
      },
      Message: {
        Body: {
          Html: {
            Data: content
          }
        },
        Subject: {
          Data: subject
        }
      },
      Source: from
    }

    return new Promise((resolve, reject) => {
      SES.sendEmail(params, (error, data) => {
        if (error) {
          return reject(error)
        }
        return resolve(data)
      })
    })
  }
}

module.exports = SESClient
