const { random } = require('faker')
const { expect } = require('chai')
const { mock, match, stub } = require('sinon')

const sesClient = require('./SESClient')

describe('SESClient', () => {
  describe('#sendEmail', () => {
    const from = random.uuid()
    const destinations = [random.uuid()]
    const subject = random.uuid()
    const content = random.uuid()
    const awsRegion = random.word()

    const params = {
      Destination: {
        ToAddresses: destinations
      },
      Message: {
        Body: {
          Html: {
            Data: content
          }
        },
        Subject: {
          Data: subject
        }
      },
      Source: from
    }

    const dependencies = {
      AWS: {
        SES: mock()
      }
    }

    let callbackValue
    let responseResult

    describe('With success result', () => {
      before('mock dependecies and call #sendmail', () => {
        callbackValue = random.uuid()

        dependencies.AWS.SES = stub(dependencies.AWS.SES, 'constructor').returns({
          sendEmail: mock()
            .withExactArgs(params, match.func)
            .callsArgWith(1, null, callbackValue)
        })

        return sesClient
          .sendEmail({ from, destinations, subject, content, awsRegion }, dependencies)
          .then(result => {
            responseResult = result
          })
      })

      it('then return a data response', () => expect(responseResult).to.deep.equal(callbackValue))
    })

    describe('With error result', () => {
      before('mock dependecies and call #sendmail', () => {
        callbackValue = random.uuid()

        dependencies.AWS.SES = stub(dependencies.AWS.SES, 'constructor').returns({
          sendEmail: mock()
          .once()
          .withExactArgs(params, match.func)
          .callsArgWith(1, callbackValue, null)
        })

        return sesClient
          .sendEmail({ from, destinations, subject, content, awsRegion }, dependencies)
          .catch(error => {
            responseResult = error
          })
      })

      it('then return a error response', () => expect(responseResult).to.deep.equal(callbackValue))
    })
  })
})
