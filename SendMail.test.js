const sinon = require('sinon')
const { expect } = require('chai')
const { internet, lorem } = require('faker')

const sendMail = require('./SendMail.js')

describe('sendMail', () => {
  const email = {
    to: internet.email(),
    subject: 'A Subject',
    from: internet.email()
  }

  const awsRegion = lorem.words()
  const content = 'Email Content'
  let dependencies

  beforeEach('prepare mocks and make the call', () => {
    dependencies = {
      SESClient: {
        sendEmail: sinon.mock()
      }
    }

    return sendMail({ email,
      content,
      awsRegion
    }, dependencies)
  })

  it('calls SESClient to send email', () => {
    const [firstCall] = dependencies.SESClient.sendEmail.getCalls()

    expect(firstCall.args).to.deep.equal([{
      content: 'Email Content',
      destinations: [email.to],
      from: email.from,
      subject: 'A Subject',
      awsRegion
    }])
  })
})
