const dependencies = {
  SESClient: require('./AWS/SESClient')
}

module.exports = function SendMail ({
  email: {
    to,
    subject,
    from
  },
  content,
  awsRegion
}, injection) {
  const {
    SESClient
  } = Object.assign({}, dependencies, injection)

  return SESClient.sendEmail({
    from,
    destinations: [
      to
    ],
    subject,
    content,
    awsRegion
  })
}
